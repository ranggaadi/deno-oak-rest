import { Relationships } from 'https://deno.land/x/denodb/mod.ts';
import Student from './students.ts';
import Lecturer from './lecturers.ts';

const StudentLecturer = Relationships.manyToMany(Student, Lecturer)

export default StudentLecturer