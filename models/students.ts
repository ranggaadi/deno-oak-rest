import {Model, DataTypes, Relationships} from 'https://deno.land/x/denodb/mod.ts';
import Departement from './departements.ts'
import Lecturer from './lecturers.ts';

class Student extends Model {
    static table = 'students';
    static timestamps = true;

    static fields = {
        id: {
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            length: 100,
        },
        generation: {
            type: DataTypes.INTEGER,
            allowNull: false,
            length:4,
        },
        departementId: Relationships.belongsTo(Departement),
    };

    static defaults = {
        generation: 2018
    }

    static departement(){
        return this.hasOne(Departement)
    }

    static lecturers(){
        return this.hasMany(Lecturer)
    }
}

export default Student