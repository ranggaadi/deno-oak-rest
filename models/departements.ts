import {Model, DataTypes} from 'https://deno.land/x/denodb/mod.ts';
import Student from './students.ts'
import Lecturer from './lecturers.ts';

class Departement extends Model {
    static table = 'departements';
    static timestamps = true;

    static fields = {
        id: {
            primaryKey: true,
            autoIncrement: true,
        },
        dept_name: {
            type: DataTypes.STRING,
            allowNull: false,
            length: 150,
        },
    };
    
    static students() {
        return this.hasMany(Student)
    }

    static lecturers(){
        return this.hasMany(Lecturer)
    }
}

export default Departement