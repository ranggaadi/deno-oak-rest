import {Model, DataTypes, Relationships} from 'https://deno.land/x/denodb/mod.ts';
import Departement from './departements.ts';
import Student from './students.ts';

class Lecturer extends Model {
    static table = 'lecturers';
    static timestamps = true;
    
    static fields = {
        id: {
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            length: 150,
        },
        course: {
            type: DataTypes.STRING,
            allowNull: false,
            length: 150,
        },
        departementId: Relationships.belongsTo(Departement)
    };
    
    
    static defaults = {
        course: "-"
    }
    
    static departement(){
        return this.hasOne(Departement)
    }

    static students() {
        return this.hasMany(Student)
    }
}

export default Lecturer