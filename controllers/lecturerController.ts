import Lecturer from "../models/lecturers.ts"
import StudentLecturer from "../models/studentLecturerRel.ts";

const getAllLecturer = async ({ response }: { response: any }) => {
    const lecturers = await Lecturer.all();
    response.body = {
        status: lecturers.length ? "success" : "fail",
        data: lecturers.length ? lecturers : "No data"
    }
}

const getLecturer = async ({ params, response }: { params: { id: string }, response: any }) => {
    const lecturer: Lecturer | undefined = await Lecturer.find(params.id)
    response.body = {
        status: lecturer ? "success" : "fail",
        data: lecturer ? lecturer : "No data"
    }
}

const createLecturer = async ({ request, response }: { request: any, response: any }) => {
    if (!request.hasBody) {
        response.status = 400
        response.body = {
            status: "fail",
            message: "Request has no body"
        }
    } else {
        const body = await request.body()

        const created = await Lecturer.create(body.value)
        if(body.value.studentId){
            await StudentLecturer.create({lecturerId: created.lastInsertId, studentId: body.value.studentId})
        }
        const currentAdded : Lecturer | undefined = await Lecturer.find(created.lastInsertId)
        response.body = {
            status: "success",
            data: currentAdded
        }
    }
}

const updateLecturer = async({params, request, response} : {params: {id: string}, request: any, response: any}) => {
    if (!request.hasBody){
        response.status = 400
        response.body = {
            status: "fail",
            message: "Request has no body"
        }
    }

    const body = await request.body()
    const currentModel : Lecturer | undefined = await Lecturer.find(params.id)
    
    if(!currentModel){
        response.status = 400
        response.body = {
            status: "fail",
            message: "rows with following id is not found"
        }
    }
    
    const info = await Lecturer.where("id", params.id).update(body.value)
    const updatedModel : Lecturer | undefined = await Lecturer.find(params.id)

    response.body = {
        status: info.affectedRows ? "success" : "fail",
        data: info.affectedRows ? updatedModel : "No data were updated"
    }
}

const deleteLecturer = async ({params, response} : {params: {id: string}, response: any}) => {
    const currentDeleted : Lecturer | undefined = await Lecturer.find(params.id)
    const info = await Lecturer.deleteById(params.id)
    if(info.affectedRows){
        await StudentLecturer.where("lecturerId", params.id).delete();
    }
    
    response.body = {
        status: info.affectedRows ? "success" : "fail",
        data: info.affectedRows ? currentDeleted : "No data" 
    }
}

export default {getAllLecturer, getLecturer, createLecturer, updateLecturer, deleteLecturer}