import Departement from "../models/departements.ts"

const getAllDepartement = async ({ response }: { response: any }) => {
    const deps = await Departement.all();
    response.body = {
        status: deps.length ? "success" : "fail",
        data: deps.length ? deps : "No data"
    }
}

const getDepartement = async ({ params, response }: { params: { id: string }, response: any }) => {
    const dep: Departement | undefined = await Departement.find(params.id)
    response.body = {
        status: dep ? "success" : "fail",
        data: dep ? dep : "No data"
    }
}

const createDepartement = async ({ request, response }: { request: any, response: any }) => {
    if (!request.hasBody) {
        response.status = 400
        response.body = {
            status: "fail",
            message: "Request has no body"
        }
    } else {
        const body = await request.body()

        const created = await Departement.create(body.value)
        const currentAdded : Departement | undefined = await Departement.find(created.lastInsertId)
        response.body = {
            status: "success",
            data: currentAdded
        }
    }
}

const updateDepartement = async({params, request, response} : {params: {id: string}, request: any, response: any}) => {
    if (!request.hasBody){
        response.status = 400
        response.body = {
            status: "fail",
            message: "Request has no body"
        }
    }

    const body = await request.body()
    const currentModel : Departement | undefined = await Departement.find(params.id)
    
    if(!currentModel){
        response.status = 400
        response.body = {
            status: "fail",
            message: "rows with following id is not found"
        }
    }
    
    const info = await Departement.where("id", params.id).update(body.value)
    const updatedModel : Departement | undefined = await Departement.find(params.id)

    response.body = {
        status: info.affectedRows ? "success" : "fail",
        data: info.affectedRows ? updatedModel : "No data were updated"
    }
}

const deleteDepartement = async ({params, response} : {params: {id: string}, response: any}) => {
    const currentDeleted : Departement | undefined = await Departement.find(params.id)
    const info = await Departement.deleteById(params.id)
    
    response.body = {
        status: info.affectedRows ? "success" : "fail",
        data: info.affectedRows ? currentDeleted : "No data" 
    }
}

export default {getAllDepartement, getDepartement, createDepartement, updateDepartement, deleteDepartement}