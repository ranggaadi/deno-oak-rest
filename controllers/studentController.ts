import Student from "../models/students.ts"
import StudentLecturer from "../models/studentLecturerRel.ts";

const getAllStudent = async ({ response }: { response: any }) => {
    const students = await Student.all();
    response.body = {
        status: students.length ? "success" : "fail",
        data: students.length ? students : "No data"
    }
}

const getStudent = async ({ params, response }: { params: { id: string }, response: any }) => {
    const student: Student | undefined = await Student.find(params.id)
    response.body = {
        status: student ? "success" : "fail",
        data: student ? student : "No data"
    }
}

const createStudent = async ({ request, response }: { request: any, response: any }) => {
    if (!request.hasBody) {
        response.status = 400
        response.body = {
            status: "fail",
            message: "Request has no body"
        }
    } else {
        const body = await request.body()

        const created = await Student.create(body.value)
        if(body.value.lecturerId){
            await StudentLecturer.create({studentId: created.lastInsertId, lecturerId: body.value.lecturerId})
        }
        const currentAdded : Student | undefined = await Student.find(created.lastInsertId)
        response.body = {
            status: "success",
            data: currentAdded
        }
    }
}

const updateStudent = async({params, request, response} : {params: {id: string}, request: any, response: any}) => {
    if (!request.hasBody){
        response.status = 400
        response.body = {
            status: "fail",
            message: "Request has no body"
        }
    }

    const body = await request.body()
    const currentModel : Student | undefined = await Student.find(params.id)
    
    if(!currentModel){
        response.status = 400
        response.body = {
            status: "fail",
            message: "rows with following id is not found"
        }
    }
    
    const info = await Student.where("id", params.id).update(body.value)
    const updatedModel : Student | undefined = await Student.find(params.id)

    response.body = {
        status: info.affectedRows ? "success" : "fail",
        data: info.affectedRows ? updatedModel : "No data were updated"
    }
}

const deleteStudent = async ({params, response} : {params: {id: string}, response: any}) => {
    const currentDeleted : Student | undefined = await Student.find(params.id)
    const info = await Student.deleteById(params.id)
    if(info.affectedRows){
        await StudentLecturer.where("studentId", params.id).delete();
    }
    
    response.body = {
        status: info.affectedRows ? "success" : "fail",
        data: info.affectedRows ? currentDeleted : "No data" 
    }
}

export default { getAllStudent, getStudent, createStudent, deleteStudent, updateStudent}