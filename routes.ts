import { Router } from "https://deno.land/x/oak/mod.ts";
import StudentController from "./controllers/studentController.ts"
import DepartementController from "./controllers/departementController.ts"
import LecturerController from "./controllers/lecturerController.ts"

const router = new Router();

router.get("/api/v1/departements", DepartementController.getAllDepartement)
    .get("/api/v1/departements/:id", DepartementController.getDepartement)
    .post("/api/v1/departements", DepartementController.createDepartement)
    .put("/api/v1/departements/:id", DepartementController.updateDepartement)
    .delete("/api/v1/departements/:id", DepartementController.deleteDepartement)

router.get("/api/v1/students", StudentController.getAllStudent)
    .get("/api/v1/students/:id", StudentController.getStudent)
    .post("/api/v1/students", StudentController.createStudent)
    .delete("/api/v1/students/:id", StudentController.deleteStudent)
    .put("/api/v1/students/:id", StudentController.updateStudent)

router.get("/api/v1/lecturers/", LecturerController.getAllLecturer)
    .get("/api/v1/lecturers/:id", LecturerController.getLecturer)
    .post("/api/v1/lecturers", LecturerController.createLecturer)
    .put("/api/v1/lecturers/:id", LecturerController.updateLecturer)
    .delete("/api/v1/lecturers/:id", LecturerController.deleteLecturer)

export default router;
