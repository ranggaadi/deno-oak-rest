import { Application } from "https://deno.land/x/oak/mod.ts";
import {Database} from 'https://deno.land/x/denodb/mod.ts';
import { config } from "https://deno.land/x/dotenv/mod.ts";
import router from './routes.ts';
import Departement from './models/departements.ts'
import Student from './models/students.ts'
import Lecturer from "./models/lecturers.ts";
import StudentLecturer from "./models/studentLecturerRel.ts";

const db = new Database('mysql', {
    host: config().DB_HOST,
    username: config().DB_USERNAME,
    password: config().DB_PASSWORD,
    database: config().DB_NAME,
})

db.link([Departement, Student, Lecturer, StudentLecturer]);
await db.sync({drop: true});

const port = 8000
const app = new Application();

app.use(router.routes())
app.use(router.allowedMethods())

console.log(`Server running on port ${port}`)
await app.listen({port})